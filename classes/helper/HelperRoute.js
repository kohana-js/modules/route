module.exports = {
  execute: async (Controller, request) => new Controller(request).execute(),
};
