const fs = require('fs');
const path = require('path');

const { KohanaJS, addNodeModule} = require('kohanajs');
addNodeModule(__dirname);

module.exports = {
  RouteList: require('./classes/RouteList'),
  HelperRoute: require('./classes/helper/HelperRoute'),
  RouterAdapter: require('./classes/RouteAdapter'),
  loadModuleRoutes: () => {
    KohanaJS.nodePackages.forEach(x => {
      const filePath = path.normalize(`${x}/routes.js`);
      if (!fs.existsSync(filePath)) return;
      require(filePath);
    });

    // activate init.js in modules
    KohanaJS.bootstrap.modules.forEach(x => {
      const filePath = path.normalize(`${KohanaJS.MOD_PATH}/${x}/routes.js`);
      if (!fs.existsSync(filePath)) return;
      require(filePath);
    });
  }
};
