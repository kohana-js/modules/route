# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [6.1.0](https://gitlab.com/kohanajs-modules/route/compare/v6.0.0...v6.1.0) (2022-03-15)


### Features

* add loadModuleRoutes ([89ea75a](https://gitlab.com/kohanajs-modules/route/commit/89ea75ae559a193826612b85ec0ec83505c266c7))

## [6.0.0](https://gitlab.com/kohanajs-modules/route/compare/v5.0.2...v6.0.0) (2022-03-09)


### ⚠ BREAKING CHANGES

* use routeMap and remove RouteList.routes

* use routeMap and remove RouteList.routes ([3b4db3b](https://gitlab.com/kohanajs-modules/route/commit/3b4db3b85d7f0e58e74f54466cff9671a1e2f432))

### [5.0.2](https://gitlab.com/kohanajs-modules/route/compare/v5.0.1...v5.0.2) (2021-10-29)


### Bug Fixes

* routes re-add in init.js ([0897a41](https://gitlab.com/kohanajs-modules/route/commit/0897a417ff793fa358376eb85ae0ca7bff32a8a5))

### 5.0.1 (2021-10-29)


### Bug Fixes

* cannot remove route ([000627e](https://gitlab.com/kohanajs-modules/route/commit/000627e00dd04eb14874201904bf59880991beae))

## [5.0.0] - 2021-09-08
### Removed
- move Route Adapter to @kohanajs/platform-web-*

## [4.0.2] - 2021-09-07
### Added
- create CHANGELOG.md

[Unreleased]: https://github.com/kohana-js/route/-/compare